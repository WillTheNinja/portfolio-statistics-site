<?php
include_once 'Core/init.php';
?>
<!DOCTYPE html>
 <html>
  <head>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title>Server - <?php echo Config::SERVER_NAME; ?></title>
    <link rel="icon" type="image/png" href="../Assets/img/favicon.ico">
    <link href="../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../Assets/css/custom.css" rel="stylesheet"> 
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">

  	<?php include_once 'Core/menu.php'; ?>

	<div class="container" role="main" style="margin-top: 30px;">
	<?php if(Config::LOGO != '') echo '<div class="col-lg-12"><center><img style="max-width: 100%;"  src="../' . Config::LOGO . '"/></center></div><br>'; ?>
		<div class="row">
			<div class="col-lg-12">
				<?php
					$sendServer = $_REQUEST['server'];
					
					foreach ($Servers as $server): if($sendServer == $server['PageName']):

					$Timer = MicroTime( true );

					//Minecraft Query Code By xPaw (https://github.com/xPaw/PHP-Minecraft-Query)
					$Query = new MinecraftQuery;

					try {
						$Query->Connect( $server['IP'], $server['Port'], 1 );
					} catch( MinecraftQueryException $e ) {
						//$Exception = $e;
					}

					$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );

					$ServerInfo = $Query->GetInfo();
					$ServerPlayers = $Query->GetPlayers();
	    		?>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title">Server: <?php echo $server['Name']; ?> (<?php echo "{$ServerInfo['Players']}/{$ServerInfo['MaxPlayers']}"; ?>)</h3>
					</div>

					<div class="panel-body">
						<div class="col-lg-4">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Server Players</h3>
								</div>

								<div class="panel-body">
									<?php
										if($ServerPlayers != 0 || $ServerPlayers != null){
											foreach( $ServerPlayers as $Player ){
												echo '<a href="../profile/' . htmlspecialchars($Player) . '"><img style="padding: 2px;" src="https://thecrafters.net/stats/avatar/' . htmlspecialchars($Player) . '/36" /></a>';
											}
										}
									?>
								</div>
							</div>
						</div>

						<div class="col-lg-8">
							<div class="panel panel-info">
								<div class="panel-heading">
									<h3 class="panel-title">Server Description</h3>
								</div>

								<div class="panel-body">
									<?php echo $server['Desc']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<?php include_once 'Core/footer.php'; ?>
	</body>

	<script src="../Assets/js/jquery.min.js"></script>
	<script src="../Assets/js/modal.js"></script>
	<script src="../Assets/js/custom.js"></script>
	<script src="../Assets/js/bootstrap.min.js"></script>
</html>