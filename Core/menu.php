<?php
include_once 'init.php';
echo '<div class="navbar navbar-default">';
	echo '<div class="navbar-header">';
	echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
  	echo '</button>';
    if(basename($_SERVER['PHP_SELF']) == 'players.php' || basename($_SERVER['PHP_SELF']) == 'servers.php'){
      echo '<a class="navbar-brand" href="../index.php">'. Config::SERVER_NAME .'</a>';
    } else {
      echo '<a class="navbar-brand" href="index.php">'. Config::SERVER_NAME .'</a>';
    }
echo '</div>';
echo '<div class="navbar-collapse collapse navbar-responsive-collapse">';
    if(basename($_SERVER['PHP_SELF']) == 'players.php' || basename($_SERVER['PHP_SELF']) == 'servers.php'){
      echo '<ul class="nav navbar-nav">';
        echo '<li><a href="../index"><i class="fa fa-home"></i> Home</a></li>';
        echo '<li><a href="../top10"><i class="fa fa-plus-circle"></i> Top 10 Online Users</a></li>';
        echo '<li><a href="../toptokens"><i class="fa fa-ticket"></i> Top 10 Token Owners</a></li>';
        echo '<li><a href="../voters"><i class="fa fa-ticket"></i> Top Voters</a></li>';
      echo '</ul>';

      echo '<form class="navbar-form navbar-right" id="searchUser" action="../playersearch" method="post">';
        echo '<input type="text" name="username" class="form-control col-lg-8" placeholder="Search Username">';
        echo '<input class="btn btn-success" type="submit" value="Search User"/>';
      echo '</form>';
    } else {
      echo '<ul class="nav navbar-nav">';
        echo '<li><a href="index"><i class="fa fa-home"></i> Home</a></li>';
        echo '<li><a href="top10"><i class="fa fa-plus-circle"></i> Top 10 Online Users</a></li>';
        echo '<li><a href="toptokens"><i class="fa fa-ticket"></i> Top 10 Token Owners</a></li>';
        echo '<li><a href="voters"><i class="fa fa-ticket"></i> Top Voters</a></li>';
      echo '</ul>';

      echo '<form class="navbar-form navbar-right" id="searchUser" action="playersearch" method="post">';
        echo '<input type="text" name="username" class="form-control col-lg-8" placeholder="Search Username">';
        echo '<input class="btn btn-success" type="submit" value="Search User"/>';
      echo '</form>';
    }
echo '</div>';
echo '</div>';
?>