<?php
include_once 'Config.php';
include_once 'DB.php';
include_once 'MinecraftQuery.class.php';

// Database instance
$db = DB::getInstance();

$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

// Gets the newest players
//$newestPlayers = $db->query("SELECT * FROM " . Config::DB_TABLE . " ORDER BY ID DESC LIMIT " . Config::NEW_PLAYERS);
//$total = $newestPlayers->first()->ID;

// Gets the top 10 online players
//$top10 = $db1->query("SELECT * FROM " . Config::DB_TABLE . " ORDER BY OnlineTime DESC LIMIT " . Config::Top10Players);

//error_reporting( E_ALL | E_STRICT );
//ini_set( 'display_errors', true );

$Timer = MicroTime( true );

//Minecraft Query Code By xPaw (https://github.com/xPaw/PHP-Minecraft-Query)
$Query = new MinecraftQuery;

try
{
	$Query->Connect( Config::SERVER_IP, Config::SERVER_PORT, 1 );
}
catch( MinecraftQueryException $e )
{
	$Exception = $e;
}

$Timer = Number_Format( MicroTime( true ) - $Timer, 4, '.', '' );

$QueryServer = $Query->GetInfo();


//Conversion of seconds to time (Google's Version)
function secondsToTime($seconds) {
    $dtF = new DateTime("@0");
    $dtT = new DateTime("@$seconds");
    return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
}

//Conversion of seconds to time (Darkcop's Version)
function secondTime($seconds){
	$hoursBefore = $seconds / 60;
	$hours = explode(".", $hoursBefore);
	$minutes = $seconds % 60;

	return $hours[0] . " Hours and " . $minutes . " Minutes";
}

function hourCheck($seconds){
	$hoursBefore = $seconds / 60;
	$hours = explode(".", $hoursBefore);
	$minutes = $seconds % 60;

	return $hours[0];
}