<?php 

class Config
{
	const   DB_USERNAME = "PlayerData_Admin",
			DB_PASSWORD = "zJXXCnW4YptU9pvu",
			DB_HOST     = "192.95.30.66:3306",
			DB_NAME     = "PlayerData",
			DB_TABLE    = "BungeeStats",
			DB_TABLE_TOKENS = "Player_Tokens",
			DB_TABLE_USERS = "BungeeUsers",
			DB_TABLE_SKYWARS = "swreloaded_player",
			DB_TABLE_SURVIVALGAMES = "HGStats",
			DB_TABLE_TOP_VOTERS = "HubVotes",
			SERVER_NAME = "The Crafters Network",
			SERVER_IP   = "Hub.TheCrafters.net",
			LOGO        = "Assets/img/Logo.png",
			SERVER_PORT = 25565,
			NEW_PLAYERS = 5,
			Top10Players = 10,
			Top10Tokens = 10,
			MAIN_WEBSITE = "",
			ENABLE_ABOUTUS = false,
			INFO_ONE    = "
					       Welcome to the Crafters Minecraft Network! 
						   Here at the Crafters we have tried very hard to custom make a network of minecraft servers that will be an enjoyable experience for players of all ages/skill level. 
						   No matter if you like factions, creative, or mini-games, there is a place for you here! 
						   Our creative server is our most popular server with an average of 40 people. 
						   But don't be scared, although there are many people, it still has the small server community vibe. 
					       Whether you want to build a giant mansion with friends or participate in a role-play with people , there will always be something for you to do on the server. 
					       Our second most popular server is our factions server. 
					       Build a giant empire and crush your enemies on the factions server, while still being met with respecful players and staff and an enjoyable environment. 
					       If raiding and pvp aren't really your thing but you don't want to play creative, we have a survival server that will be perfect for you! 
					       Claim some land and start surviving with your friends without having to worry about being raided or killed.
						   ",
			INFO_TWO    = "
						   Here at the crafters, we have multiple mini-game modes available for you to enjoy. 
						   We offer game modes such as prison, kitpvp, skywars, and skyblock. 
					       With such a variety of game modes we hope that you will always find something to do with friends. 
					       Prove your PvP mastery on our kitpvp with a large arsenal of kits at your disposal. 
					       If you are looking for a more challenging game mode, you can try out our prison server. 
					       Work your way to the top by mining materials until you can finally break out of the prison! 
					       Skyblock is perfect for you if you want a challenging survival-like game mode. 
					       Live by yourself on your island, or invite or friends and make an island that players will be jealous of for years to come. 
					       Our next server is the perfect mix between kitpvp and skyblock. That server is skywars. 
					       Skywars is a custom made plugin designed to give you the best possible experience. 
					       Battle other until there is only one standing. Build a bridge, or use ender pearls to get to the middle island for more chests with better loot. 
					       No matter what kind of player you are, there will alwaysbe a place for you on the Crafters Network.
					      ";
}

$Servers                = [
								[
									"BackgroundImage" => "Assets/img/servers/factions.png",
									"Name"            => "Factions",
									"PageName"            => "factions",
									"IP"            => "198.27.81.85",
									"Port"            => "25566",
									"Desc"            => "A very long story about factions, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/creative.png",
									"Name"            => "Creative",
									"PageName"            => "creative",
									"IP"            => "198.27.81.85",
									"Port"            => "25567",
									"Desc"            => "A very long story about creative, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/skyblock.png",
									"Name"            => "SkyBlock",
									"PageName"            => "skyblock",
									"IP"            => "192.99.147.170",
									"Port"            => "25584",
									"Desc"            => "A very long story about skyblock, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/survival.png",
									"Name"            => "Survival",
									"PageName"            => "survival",
									"IP"            => "192.99.147.170",
									"Port"            => "25551",
									"Desc"            => "A very long story about survival, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/skywars.png",
									"Name"            => "SkyWars",
									"PageName"            => "skywars",
									"IP"            => "192.99.147.170",
									"Port"            => "25585",
									"Desc"            => "A very long story about skywars, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/prison.png",
									"Name"            => "Prison",
									"PageName"            => "prison",
									"IP"            => "192.99.147.170",
									"Port"            => "25567",
									"Desc"            => "A very long story about prison, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/kitpvp.png",
									"Name"            => "KitPvP",
									"PageName"            => "kitpvp",
									"IP"            => "192.95.30.66",
									"Port"            => "26973",
									"Desc"            => "A very long story about kitpvp, maybe?"
								],
								[
									"BackgroundImage" => "Assets/img/servers/tekkitclassic.png",
									"Name"            => "Tekkit Classic",
									"PageName"            => "tekkitclassic",
									"IP"            => "192.99.242.118",
									"Port"            => "25565",
									"Desc"            => "A very long story about tekkit classic, maybe?"
								]
							];	