<?php
include_once '././Core/init.php';
$top10 = $db->query("SELECT * FROM " . Config::DB_TABLE_TOP_VOTERS . " ORDER BY voteCount DESC LIMIT 15");
$currentYear = date("Y");
?>
<div class="col-lg-6">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title" style="text-align:center;">Top 15 Voters For <?php echo date("F") . " " . $currentYear;?></h3>
		</div>
		<table style="width:100%;">
		  <tr>
		    <td style="text-align:center;"><h4><span class="label label-primary">Rank</span></h4></td>
		    <td style="text-align:center;"><h4><span class="label label-primary">Player</span></h4></td> 
		    <td style="text-align:center;"><h4><span class="label label-primary">Votes</span></h4></td>
		  </tr>
		
          <?php $number = 1; foreach($top10->results() as $object): ?>
		  <tr>
		    <td style="text-align:center;"><?php echo $number; ?></td>
		    <td style="text-align:center;"><a id="removeFormatting" href="profile/<?php echo $object->Player; ?>"><?php echo $object->Player; ?></a></td> 
		    <td style="text-align:center;"><?php echo $object->voteCount; ?></td>
		  </tr>
		  <?php $number++; ?>
          <?php endforeach;?>
      	</table>
	</div>
</div>