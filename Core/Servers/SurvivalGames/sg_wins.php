<?php
include_once '././Core/init.php';

$SGConnection = $db->query("SELECT * FROM " . Config::DB_TABLE_SURVIVALGAMES . " WHERE uuid = ?", array($playerUUID));
if(!$SGConnection->count() == 0){
	$SGResults = $SGConnection->first();
	$kills = $SGResults->Kills;
	$wins = $SGResults->Wins;
} else {
	$kills = "0";
	$wins = "0";
}
?>

<?php for($c = 0; $c <= 9; $c++): ?>
<?php
$win_amount = array("10", "20", "30", "40", "50", "60", "70", "80", "90", "100");
?>
<?php if($wins >= $win_amount[$c]): ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="Win <?php echo $win_amount[$c]; ?> Games" data-content="<i class='fa fa-check'></i> Won <?php echo $win_amount[$c]; ?> Games"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php else: ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="Win <?php echo $win_amount[$c]; ?> Games" data-content="<i class='fa fa-times'></i> Win <?php echo $win_amount[$c]; ?> Games"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php endif; ?>
<?php endfor; ?>