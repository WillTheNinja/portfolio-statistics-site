<?php
include_once '././Core/init.php';

$SkywarsConnection = $db->query("SELECT * FROM " . Config::DB_TABLE_SKYWARS . " WHERE uuid = ?", array($playerUUID));
if(!$SkywarsConnection->count() == 0){
$SkywarsResults = $SkywarsConnection->first();
$score = $SkywarsResults->score;
$gamesplayed = $SkywarsResults->games_played;
$gameswon = $SkywarsResults->games_won;
$kills = $SkywarsResults->kills;
$deaths = $SkywarsResults->deaths;
} else {
$score = "0";
$gamesplayed = "0";
$gameswon = "0";
$kills = "0";
$deaths = "0";
}
?>

<?php for($c = 0; $c <= 9; $c++): ?>
<?php
$kills_amount = array("10", "20", "30", "40", "50", "60", "70", "80", "90", "100");
?>
<?php if($kills >= $kills_amount[$c]): ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="<?php echo $kills_amount[$c]; ?> Kills" data-content="<i class='fa fa-check'></i> Kiled <?php echo $kills_amount[$c]; ?> Players"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php else: ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="<?php echo $kills_amount[$c]; ?> Kills" data-content="<i class='fa fa-times'></i> Kill <?php echo $kills_amount[$c]; ?> Players"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php endif; ?>
<?php endfor; ?>