<?php
include_once '././Core/init.php';

$SkywarsConnection = $db->query("SELECT * FROM " . Config::DB_TABLE_SKYWARS . " WHERE uuid = ?", array($playerUUID));
if(!$SkywarsConnection->count() == 0){
$SkywarsResults = $SkywarsConnection->first();
$score = $SkywarsResults->score;
$gamesplayed = $SkywarsResults->games_played;
$gameswon = $SkywarsResults->games_won;
$kills = $SkywarsResults->kills;
$deaths = $SkywarsResults->deaths;
} else {
$score = "0";
$gamesplayed = "0";
$gameswon = "0";
$kills = "0";
$deaths = "0";
}
?>

<?php for($v = 0; $v <= 9; $v++): ?>
<?php
$won_amount = array("10", "20", "30", "40", "50", "60", "70", "80", "90", "100");
?>
<?php if($gameswon >= $won_amount[$v]): ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="Win <?php echo $won_amount[$v]; ?> Games" data-content="<i class='fa fa-check'></i> Won <?php echo $won_amount[$v]; ?> Games"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php else: ?>
<div class="col-md-1" style="background-color: white; padding: 10px; margin: 10px;-moz-border-radius: 5px;border-radius: 5px;border-style: solid; border-width: 5px;">
<center>
<img data-toggle="popover" data-trigger="focus" title="Win <?php echo $won_amount[$v]; ?> Games" data-content="<i class='fa fa-times'></i> Win <?php echo $won_amount[$v]; ?> Games"  src="../Assets/img/achievements/unknown.png">
</center>
</div>
<?php endif; ?>
<?php endfor; ?>