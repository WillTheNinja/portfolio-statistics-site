<?php
include_once 'Core/init.php';
$newestPlayers = $db->query("SELECT * FROM " . Config::DB_TABLE . " ORDER BY ID DESC LIMIT " . Config::NEW_PLAYERS);
$total = $newestPlayers->first()->ID;
?>
<!DOCTYPE html>
 <html>
  <head>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title>Home - <?php echo Config::SERVER_NAME; ?></title>
    <link rel="icon" type="image/png" href="Assets/img/favicon.ico">
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/custom.css" rel="stylesheet"> 
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">

  	<?php include_once 'Core/menu.php'; ?>

	  <div class="container" role="main" style="margin-top: 30px;">
	  	<div class="container-fluid">
	  		<?php if(Config::LOGO != '') echo '<div class="col-lg-12"><center><img style="max-width: 100%;"  src="' . Config::LOGO . '"/></center></div><br>'; ?>
	    	<div class="row">
	        	<div class="col-lg-9">
	          		<div class="panel panel-primary">
	            		<div class="panel-heading">
	              			<h3 class="panel-title">Network Servers</h3>
	            		</div>
	            
	            		<div class="panel-body">
	              			<div id="ServerList">
	              			<?php foreach($Servers as $server): ?>
	              				<div class="col-lg-6">
	              					<div class="panel panel-warning">
	              						<div class="panel-heading">
	              							<h3 class="panel-title"><?php echo $server['Name']; ?></h3>
	              						</div>
	              						<div style="max-width: 100%;max-height: 367px;" class="panel-body">
												<form action="server/<?php echo $server['PageName']; ?>" method="post" id="<?php echo $server['PageName']; ?>">
												    <input type="hidden" value="<?php echo $server['PageName']; ?>" name="server">
												    <a class="thumbnail" href="javascript:{}" onclick="document.getElementById(&apos;<?php echo $server['PageName']; ?>&apos;).submit(); return false;">
												    <img src="<?php echo $server['BackgroundImage']; ?>" style="width: 100%;">
												    </a>
												</form>
	              						</div>
	              					</div>
	              				</div>
	              			<?php endforeach; ?>
	              			</div>
	            		</div>
	        		</div>
	        	</div>
	        <div class="col-lg-3">
	          <div class="panel panel-info">
	            <div class="panel-heading">
	              <h3 class="panel-title">Network Statistics</h3>
	            </div>
	            <div class="panel-body">
	              <p>Total Unique Players: <span class="label label-primary"><?php echo $total; ?></span></p>
	              <p>Total Online Players: <span class="label label-primary">
	              <?php echo "{$QueryServer['Players']}/{$QueryServer['MaxPlayers']}"; ?>
	              </span></p>
	            </div>
	          </div>
	          <div class="panel panel-success">
	            <div class="panel-heading">
	              <h3 class="panel-title">Newest Players</h3>
	            </div>
	            <div class="panel-body">
	              <ul class="list-group">
	                  <?php foreach($newestPlayers->results() as $object): ?>
	                    <form action="profile/<?php echo $object->Player; ?>" method="post" id="<?php echo $object->Player; ?>">
		                    <input type="hidden" value="<?php echo $object->Player; ?>" name="username">
		                    <a style="text-decoration: none;" href="javascript:{}" onclick="document.getElementById(&apos;<?php echo $object->Player; ?>&apos;).submit(); return false;">
		                    <li name="username" class="list-group-item">
		                    	<img style="padding-right: 10px;" src="https://thecrafters.net/stats/avatar/<?php echo $object->Player; ?>/32" alt="<?php echo $object->Player; ?>">
		                    	<?php echo $object->Player; ?>
		                    </li>
		                    </a>
	                    </form>
	                  <?php endforeach;?>
	              </ul>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <?php include_once 'Core/footer.php'; ?>
  </body>
	<script src="Assets/js/jquery.min.js"></script>
	<script src="Assets/js/modal.js"></script>
	<script src="Assets/js/custom.js"></script>
	<script src="Assets/js/bootstrap.min.js"></script>
</html>