<?php
include_once 'Core/init.php';

$playerUser  = $_REQUEST['username'];
$faviconURL  = "https://thecrafters.net/stats/avatar/$playerName/32";

$playerName = $_REQUEST['username'];

$PlayerInfoConnection = $db->query("SELECT * FROM " . Config::DB_TABLE . " WHERE Player = ?", array($playerUser));
if($PlayerInfoConnection->count() >= 1){
  $result = $PlayerInfoConnection->first();
  $playerName = $result->Player;
  $faviconURL = "https://thecrafters.net/stats/avatar/$playerName/32";
  $checks = $result->OnlineTime;
  $sTime = secondTime($checks);
  $hTime = hourCheck($checks);
  $playerUUID = $result->UUID;


  $json_url = "https://thecrafters.net/forum/api.php?action=getUser&hash=HvoOziC70J0fly8K0uTu&grab_as=$playerName";
  $json = @file_get_contents($json_url, true);
  $json = str_replace('},]',"} ]",$json);
  $data = json_decode($json);

  if($json === FALSE){
  } else {
    $mcUsername = $data->custom_fields->minecraftUserProfile;
    $league = $data->custom_fields->legendsUserProfile;
    $origin = $data->custom_fields->originUserProfile;
    $psn = $data->custom_fields->psnUserProfile;
    $twitch = $data->custom_fields->twitchUserProfile;
    $steam = $data->custom_fields->steamUserProfile;
    $twitter = $data->custom_fields->twitter;
    $xbox = $data->custom_fields->xboxliveUserProfile;
    $youtube = $data->custom_fields->youtubeUserProfile;
  }

} else if ($PlayerInfoConnection->count() === 0){
  //header("Location: ../index");
}

$PlayerConnection = $db->query("SELECT * FROM " . Config::DB_TABLE_TOKENS . " WHERE PlayerUUID = ?", array($playerUUID));
if(!$PlayerConnection->count() == 0){
  $TokenResults = $PlayerConnection->first();
  $playerTokens = $TokenResults->Tokens;
} else {
  $playerTokens = 0;
}

$BungeeUsersConnection = $db->query("SELECT * FROM " . Config::DB_TABLE_USERS . " WHERE UUID = ?", array($playerUUID));
if(!$BungeeUsersConnection->count() == 0){
  $BungeeResults = $BungeeUsersConnection->first();
  $playerGroup = $BungeeResults->PGroup;
} else {
  $playerGroup = "Player";
}

?>
<!DOCTYPE html>
 <html>
  <head>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title><?php echo $playerName; ?> - Player Statistics - <?php echo Config::SERVER_NAME; ?></title>
    <link rel="icon" type="image/png" href="<?php echo $faviconURL; ?>">
    <link href="../Assets/css/bootstrap.min.css" rel="stylesheet"> 
    <link href="../Assets/css/custom.css" rel="stylesheet"> 
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">
  
  <?php include_once 'Core/menu.php'; ?>

  <div class="container" role="main" style="margin-top: 60px;">
    <div class="row">
      <?php if($PlayerInfoConnection->count() === 0): ?>
         <h1 class="center" style="color: white;">Username '<?php echo $playerName; ?>' Not Found!</h1>
        <?php else: ?>
        <div class="col-lg-12">
          <div class="panel panel-primary">
            <div class="panel-body" id="profile_header">
              <span class="col-lg-3">
                <a href="#" class="thumbnail">
                  <img style="width: 100%;" src="<?php echo "https://thecrafters.net/stats/avatar/$playerName/230"; ?>" alt="<?php echo $playerName; ?>"/>
                </a>
              </span>
              <span class="col-lg-3">
                <h1><span class="label label-primary"><?php echo $playerName; ?></span></h1><br>  
                <h2><span class="label label-warning"><i class="fa fa-ticket"></i> Crafters Tokens: <?php echo $playerTokens; ?></span></h2>
                <?php if($result->Online === 'True'): ?>
                  <h2><span class="label label-warning"><i class="fa fa-plus-circle"></i> Server: <?php echo ucfirst($result->CurrentServer); ?></span></h2>
                <?php else: ?>
                  <h2><span id="tooltip" data-toggle="tooltip" data-placement="right" title="Last Online: <?php echo $result->LastJoined; ?> (<?php echo $result->LastJoinedTime; ?> Eastern Time)" class="label label-danger"><i class="fa fa-minus-circle"></i> Offline</span></h2>
                <?php endif; ?>
              </span>
            </div>
          </div>
        </div>



        <?php endif; ?>
    </div>
  </div>
  <br>
  <?php include_once 'Core/footer.php'; ?>
  </body>
  <script src="../Assets/js/jquery.min.js"></script>
  <script src="../Assets/js/modal.js"></script>
  <script src="../Assets/js/custom.js"></script>
  <script src="../Assets/js/bootstrap.min.js"></script>
  <script type="text/javascript">
  $("#popoverOption").popover({ trigger: "hover" });
  $("#tooltip").tooltip();
  $(function () {
    $('[data-toggle="tooltip"]').tooltip({html:true})
  })
  $(function () {
    $('[data-toggle="popover"]').popover({ trigger:"hover", html:true })
  })
  </script>
</html>