<?php
include_once 'Core/init.php';
?>
<!DOCTYPE html>
 <html>
  <head>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title>Top 10 Online Players - <?php echo Config::SERVER_NAME; ?></title>
    <link rel="icon" type="image/png" href="Assets/img/favicon.ico">
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/custom.css" rel="stylesheet"> 
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">

  	<?php include_once 'Core/menu.php'; ?>

	  <div class="container" role="main" style="margin-top: 30px;">
	  	<div class="container-fluid">
	  		<?php if(Config::LOGO != '') echo '<div class="col-lg-12"><center><img style="max-width: 100%;"  src="' . Config::LOGO . '"/></center></div><br>'; ?>
    	  	<div class="row">
    	  		<?php include_once 'Core/Servers/TopTen/players.php'; ?>
		    	<?php include_once 'Core/Servers/TopTen/staff.php'; ?>
		    	</div>
	      	</div>
	    </div>
	  </div>
	  <?php include_once 'Core/footer.php'; ?>
  </body>
  	<script src="Assets/js/jquery.min.js"></script>
	<script src="Assets/js/modal.js"></script>
	<script src="Assets/js/custom.js"></script>
	<script src="Assets/js/bootstrap.min.js"></script>
	<!--<span id="popoverOption" class="btn" href="#" data-content="Popup with option trigger" rel="popover" data-placement="bottom" data-original-title="<?php echo $object->Player; ?>"><?php echo $object->Player; ?></span>-->
  <script type="text/javascript">
	$("#popoverOption").popover({ trigger: "hover" });
	$("#tooltip").tooltip();
  </script>
</html>