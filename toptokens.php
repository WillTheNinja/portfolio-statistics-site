<?php
include_once 'Core/init.php';
$top10 = $db->query("SELECT * FROM " . Config::DB_TABLE_TOKENS . " ORDER BY Tokens DESC LIMIT " . Config::Top10Tokens);
?>
<!DOCTYPE html>
 <html>
  <head>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
    <title>Top 10 Tokens Owners - <?php echo Config::SERVER_NAME; ?></title>
    <link rel="icon" type="image/png" href="Assets/img/favicon.ico">
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="Assets/css/custom.css" rel="stylesheet"> 
    <link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body role="document">

  	<?php include_once 'Core/menu.php'; ?>

	  <div class="container" role="main" style="margin-top: 30px;">
	  	<div class="container-fluid">
	  		<?php if(Config::LOGO != '') echo '<div class="col-lg-12"><center><img style="max-width: 100%;"  src="' . Config::LOGO . '"/></center></div><br>'; ?>
    	  	<div class="row">
		    	<div class="col-lg-12">
		      		<div class="panel panel-primary">
		        		<div class="panel-heading">
		          			<h3 class="panel-title" style="text-align:center;">Top 10 Token Owners</h3>
		        		</div>
							<table style="width:100%;">
							  <tr>
							    <td style="text-align:center;"><h4><span class="label label-primary">Rank</span></h4></td>
							    <td style="text-align:center;"><h4><span class="label label-primary">Player</span></h4></td> 
							    <td style="text-align:center;"><h4><span class="label label-primary">Total Tokens</span></h4></td>
							  </tr>
							
			        		<div class="panel-body">
			                  <?php $number = 1; foreach($top10->results() as $object): ?>
							  <tr>
							    <td style="text-align:center;"><?php echo $number; ?></td>
							    <td style="text-align:center;"><a id="removeFormatting" href="profile/<?php echo $object->PlayerName; ?>"><?php echo $object->PlayerName; ?></a></td> 
							    <td style="text-align:center;"><?php echo $object->Tokens; ?></td>
							  </tr>
							  <?php $number++; ?>
			                  <?php endforeach;?>
			              	</table>
		        		</div>
		    		</div>
		    	</div>
	      	</div>
	    </div>
	  </div>
	  <?php include_once 'Core/footer.php'; ?>
  </body>
  	<script src="Assets/js/jquery.min.js"></script>
	<script src="Assets/js/modal.js"></script>
	<script src="Assets/js/custom.js"></script>
	<script src="Assets/js/bootstrap.min.js"></script>
	<!--<span id="popoverOption" class="btn" href="#" data-content="Popup with option trigger" rel="popover" data-placement="bottom" data-original-title="<?php echo $object->Player; ?>"><?php echo $object->Player; ?></span>-->
  <script type="text/javascript">
	$("#popoverOption").popover({ trigger: "hover" });
	$("#tooltip").tooltip();
  </script>
</html>